﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TelekinesisObjectController : MonoBehaviour
{
    /// <summary> Rigidbody attached to this object </summary>
    internal Rigidbody ObjectRigidbody;

    /// <summary> Is telekinesis used on this object now </summary>
    internal bool IsDuringTelekinesis = false;

    /// <summary> Configurable joint created for telekinesis </summary>
    private ConfigurableJoint configurableJoint;


    // Use this for initialization
    void Start()
    {
        ObjectRigidbody = GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Set all values needed for object to do telekinesis, creates and sets Configurable Joint
    /// </summary>
    /// <param name="telekinesisHelper"></param>
    public void StartedTelekinesis(Transform telekinesisHelper)
    {
        ObjectRigidbody = GetComponent<Rigidbody>();

        /* Creating and setting ConfigurableJoint */

        configurableJoint = gameObject.AddComponent<ConfigurableJoint>();
        configurableJoint.connectedBody = telekinesisHelper.GetComponent<Rigidbody>();
        configurableJoint.autoConfigureConnectedAnchor = false;
        configurableJoint.connectedAnchor = Vector3.zero;
        configurableJoint.anchor = Vector3.zero;

        /* Set joint for Object to return to his target position */
        JointDrive drive = new JointDrive
        {
            positionSpring = 7500,
            maximumForce = 10000
        };
        configurableJoint.xDrive = drive;
        configurableJoint.yDrive = drive;
        configurableJoint.zDrive = drive;

        ///* Set joint for Object to return to his target rotation */
        //JointDrive angularDrive = new JointDrive
        //{
        //    positionSpring = 1000,
        //    maximumForce = 10000
        //};
        //configurableJoint.angularXDrive = angularDrive;
        //configurableJoint.angularYZDrive = angularDrive;

        ObjectRigidbody.useGravity = false;
        IsDuringTelekinesis = true;
        ObjectRigidbody.isKinematic = false;
    }

    /// <summary>
    /// Release object from telekinesis
    /// </summary>
    public void Release()
    {
        /*Destroy configurable joint*/
        if (configurableJoint != null)
        {
            Destroy(configurableJoint);
        }

        IsDuringTelekinesis = false;

        ObjectRigidbody.useGravity = true;

        //Destroy(this);
    }

}
