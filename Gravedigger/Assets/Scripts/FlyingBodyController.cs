﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingBodyController : MonoBehaviour
{

    public List<Rigidbody> AllRigidbodies = new List<Rigidbody>();

    private float flyingXValue;

    private bool isFlying;

    // Start is called before the first frame update
    void Start()
    {

        AllRigidbodies = new List<Rigidbody>();
        foreach (Rigidbody rBody in GetComponentsInChildren<Rigidbody>())
        {
            AllRigidbodies.Add(rBody);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isFlying)
            return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            AddUpPower();
        }
        transform.position = new Vector3(flyingXValue, transform.position.y, transform.position.z);

        if (Time.frameCount % 10 == 0)
            CheckIfIsStillMoving();
    }

    public void StartedFlying()
    {
        flyingXValue = transform.position.x;
        isFlying = true;
    }

    public void AddUpPower()
    {
        foreach (Rigidbody rBody in AllRigidbodies)
        {
            rBody.AddForce(Vector3.up * 20, ForceMode.Impulse);
        }
    }

    public void AddTrampolinePower()
    {
        foreach (Rigidbody rBody in AllRigidbodies)
        {
            float currentPower = rBody.velocity.magnitude;
            if (currentPower < 3)
                continue;
            Debug.Log("Current power " + currentPower);
            rBody.AddForce(Vector3.up * currentPower * 3f, ForceMode.Impulse);
        }
    }

    private void CheckIfIsStillMoving()
    {
        if (AllRigidbodies == null)
            return;

        if (AllRigidbodies.Count == 0)
            return;

        float velocitiesMagnitudes = 0;
        foreach (Rigidbody rBody in AllRigidbodies)
        {
            velocitiesMagnitudes += rBody.velocity.magnitude;
        }

        velocitiesMagnitudes /= AllRigidbodies.Count;

        if (velocitiesMagnitudes < 1)
        {
            isFlying = false;
            GameUi.Get.EnableResetButton();
        }
    }
}
