﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    internal TelekinesisController Telekinesis;

    public float MoveSpeed;

    public float speedH = 2.0f;
    public float speedV = 2.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        Telekinesis = GetComponent<TelekinesisController>();
        Telekinesis.Player = this;
    }

    // Update is called once per frame
    void Update()
    {
        InputUpdate();
    }

    public void InputUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (!Telekinesis.TelekinesisInProgress)
            {
                TelekinesisObjectController telekinesisObject = CheckIfThereIsTelekinesisObject();
                if (telekinesisObject != null)
                    Telekinesis.StartTelekinesis(telekinesisObject);
            }
        }

    }

    private TelekinesisObjectController CheckIfThereIsTelekinesisObject()
    {
        RaycastHit raycastHit;
        if (Physics.Raycast(CameraController.Get.transform.position, CameraController.Get.transform.forward, out raycastHit, 10, Telekinesis.TelekinesisObjectsLayerMask, QueryTriggerInteraction.Ignore))
        {
            Rigidbody attachedRigidbody = raycastHit.collider.attachedRigidbody;
            if (attachedRigidbody != null)
            {
                TelekinesisObjectController telekinesisObject = raycastHit.collider.attachedRigidbody.GetComponent<TelekinesisObjectController>();
                if (telekinesisObject != null)
                    return telekinesisObject;
            }
        }

        return null;
    }
}
