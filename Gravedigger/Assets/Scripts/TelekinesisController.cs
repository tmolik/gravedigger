﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TelekinesisController : MonoBehaviour
{

    internal PlayerController Player;

    /// <summary> Reference to object that we use telekinesis on </summary>
    internal TelekinesisObjectController TelekinesisObject;

    /// <summary> Prefab of telekinesis helper object </summary>
    public GameObject TelekinesisHelperPrefab;

    /// <summary> How fast will telekinesis helper move to its target position </summary>
    public float ObjectMovementSharpness = 4;

    internal float CurrentObjectDistance;

    public float MinimumDistance = 3;
    public float MaximumDistance = 20;

    public LayerMask DistanceCheckLayerMask;
    public LayerMask TelekinesisObjectsLayerMask;

    internal bool TelekinesisInProgress;

    /// <summary> Reference to telekinesis helper object </summary>
    private GameObject telekinesisHelper;

    /// <summary> Current distance between object and player(camera) </summary>
    private float objectDistanceToPlayer;

    /// <summary> How much was object center offseted from screen center when telekinesis started </summary>
    private Vector3 offset;

    private Camera mainCamera;

    private Vector3 currentPosition;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
    }


    // Update is called once per frame
    public void Update()
    {
        CheckPlayerInput();
        ChangeObjectDistance();
        MoveObject();
    }

    /// <summary>
    /// Checks whether player clicked telekinesis button, how long was it pressed etc and chooses action to do after it
    /// </summary>
    private void CheckPlayerInput()
    {
        if (TelekinesisInProgress)
        {
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                StopTelekinesis();
            }
        }
    }

    /// <summary>
    /// Changes distance between object and player(well, camera). If it's set to use static distance, it lerp distance to its static value.
    /// If it's not static, it changes distance with player input
    /// </summary>
    private void ChangeObjectDistance()
    {
        float scrollValue = Input.mouseScrollDelta.y * 4;
        float clampedWantedDistance = Mathf.Clamp(CurrentObjectDistance + scrollValue, MinimumDistance, MaximumDistance);
        CurrentObjectDistance = Mathf.Lerp(CurrentObjectDistance, clampedWantedDistance, Time.deltaTime * 8);

        float targetDistance = CurrentObjectDistance;

        Vector3 origin = mainCamera.transform.position;
        Vector3 direction = mainCamera.transform.forward;

        if (TelekinesisObject == null)
            return;

        foreach (RaycastHit raycastHit in Physics.RaycastAll(origin, direction, targetDistance, DistanceCheckLayerMask, QueryTriggerInteraction.Ignore))
        {
            if (raycastHit.collider.gameObject == TelekinesisObject.gameObject)
                continue;

            float distanceToHit = Vector3.Distance(origin, raycastHit.point);

            if (distanceToHit < targetDistance)
            {
                targetDistance = distanceToHit;
            }
        }

        objectDistanceToPlayer = Mathf.Lerp(objectDistanceToPlayer, targetDistance, Time.deltaTime * 3);
    }

    /// <summary>
    /// Calculates current position for object and moves telekinesis helper to this position
    /// </summary>
    public void MoveObject()
    {
        if (telekinesisHelper == null || TelekinesisObject == null)
            return;

        /* track mouse position. */
        Vector3 currentScreenSpace = new Vector3(Screen.width / 2, Screen.height / 2, objectDistanceToPlayer);

        /* convert screen position to world position with offset changes. */
        currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + offset;

        /* Lerp telekinesis helper position to target position */
        telekinesisHelper.transform.position = Vector3.Lerp(telekinesisHelper.transform.position, currentPosition, Time.deltaTime * ObjectMovementSharpness);

        /* Set telekinesis helper rotation in player direction */
        Vector3 objectDirection = (Player.transform.position - TelekinesisObject.transform.position).normalized;
        objectDirection.y = 0;
        telekinesisHelper.transform.rotation = Quaternion.Lerp(telekinesisHelper.transform.rotation, Quaternion.LookRotation(objectDirection, Player.transform.up), Time.deltaTime * 30);
    }

    public void StartTelekinesis(TelekinesisObjectController telekinesiObject)
    {
        TelekinesisObject = telekinesiObject;

        Vector3 objectDirection = (Player.transform.position - TelekinesisObject.transform.position).normalized;
        objectDirection.y = 0;

        /*Create telekinesis helper ( invisible rigidbody that will be connected body in object hinge joint*/
        Quaternion startRotation = Quaternion.LookRotation(objectDirection, Player.transform.up);
        telekinesisHelper = GameObject.Instantiate(TelekinesisHelperPrefab, TelekinesisObject.transform.position, startRotation);

        /* Start telekinesis on object, let it create configurable joint */
        TelekinesisObject.StartedTelekinesis(telekinesisHelper.transform);

        /* Calculate current object distance and offset */
        objectDistanceToPlayer = Camera.main.WorldToScreenPoint(TelekinesisObject.transform.position).z;
        CurrentObjectDistance = objectDistanceToPlayer;
        offset = TelekinesisObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, objectDistanceToPlayer));
        TelekinesisInProgress = true;

    }

    public void StopTelekinesis()
    {
        TelekinesisInProgress = false;
        if (TelekinesisObject != null)
        {
            /* Release object from telekinesis */
            TelekinesisObject.Release();

            /* Clear object references */
            TelekinesisObject = null;
            if (telekinesisHelper != null)
                GameObject.Destroy(telekinesisHelper.gameObject);
        }
    }
}
