﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour
{

    public static MainController Get { get; private set; }

    public MainController()
    {
        Get = this;
    }

    public EGamePhase CurrentGamePhase;

    public PlayerController Player;

    public CameraController FollowRagdollCamera;

    public GameObject FlyingBodyPrefab;

    public Vector3 FlyingBodyPosition;
    public Vector3 FlyingBodyEulers;

    public CannonController Cannon;

    // Start is called before the first frame update
    void Start()
    {
        SetLookingAtCannonCamera();
    }
        
    public void SetLookingAtCannonCamera()
    {
        CurrentGamePhase = EGamePhase.LookingAtCannon;
        Player.gameObject.SetActive(false);
        FollowRagdollCamera.CameraPhase = EGamePhase.LookingAtCannon;
        FollowRagdollCamera.TargetTransform = Cannon.LookAtPoint;
        FollowRagdollCamera.gameObject.SetActive(true);
    }


    public void SetFollowingRagdollPhase(FlyingBodyController flyingBody)
    {
        CurrentGamePhase = EGamePhase.FollowingRagdoll;
        Player.gameObject.SetActive(false);
        FollowRagdollCamera.CameraPhase = EGamePhase.FollowingRagdoll;
        FollowRagdollCamera.TargetTransform = flyingBody.transform;
        FollowRagdollCamera.gameObject.SetActive(true);
    }

    public void SetFirstPersonPhase()
    {
        CurrentGamePhase = EGamePhase.FirstPerson;
        Player.gameObject.SetActive(true);
        FollowRagdollCamera.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            SetFirstPersonPhase();
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            SetLookingAtCannonCamera();
            SpawnNewFlyingBody();
        }
    }

    public void SpawnNewFlyingBody()
    {
        GameObject body = Instantiate(FlyingBodyPrefab,FlyingBodyPosition,Quaternion.Euler(FlyingBodyEulers));
    }
}
