﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    #region SINGLETON

    protected static CameraController instance;

    public static CameraController Get
    {
        get
        {
            if (instance == null)
            {
                instance = (CameraController)FindObjectOfType(typeof(CameraController));
            }

            return instance;
        }
    }

    #endregion

    public Transform TargetTransform;

    public Vector3 RagdollBodyOffset;
    public Vector3 CannonOffset;

    internal EGamePhase CameraPhase;


    // Update is called once per frame
    void Update()
    {
        if (TargetTransform == null)
            return;

        if (CameraPhase == EGamePhase.FollowingRagdoll)
            transform.position = TargetTransform.position + RagdollBodyOffset;
        else if (CameraPhase == EGamePhase.LookingAtCannon)
            transform.position = TargetTransform.position + CannonOffset;

        transform.LookAt(TargetTransform);
    }

}
