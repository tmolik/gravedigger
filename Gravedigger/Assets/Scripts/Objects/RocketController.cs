﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketController : MonoBehaviour
{
    public float Power=3;
    public float Duration = 2;

    private Rigidbody attachedRigidbody;

    private bool isWorking = false;
    private float workTimer = 0;


    // Start is called before the first frame update
    void Start()
    {
        attachedRigidbody = GetComponentInParent<Rigidbody>();        
    }

    // Update is called once per frame
    void Update()
    {
        if (attachedRigidbody != null)
        {
            if (isWorking)
            {
                attachedRigidbody.AddForce(Vector3.forward * Power, ForceMode.Acceleration);

                workTimer -= Time.deltaTime;
                if (workTimer <= 0)
                {
                    isWorking = false;
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.I))
                {
                    StartWorking();
                }
            }
        }
    }

    private void StartWorking()
    {
        workTimer = Duration;
        isWorking = true;
    }
}
