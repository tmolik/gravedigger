﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonController : MonoBehaviour
{
    public float FirePower = 100;
    public Transform ShotOrigin;
    public Transform LookAtPoint;

    private List<Rigidbody> RigidbodiesInCannon = new List<Rigidbody>();


    public float RotationRate = 10;
    private float rotationChange;

    internal bool IsRotatingByItself = false;
    private Vector2 RotationRange = new Vector2(20, 70);
    private int currentlyRotating = 1;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            Fire();
            IsRotatingByItself = false;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            IsRotatingByItself = true;
        }

        if (IsRotatingByItself)
        {
            Rotate();
        }
        else
        {
            HandlePlayerInput();
        }
    }

    private void Rotate()
    {
        Vector3 newEulers = transform.rotation.eulerAngles;
        newEulers.x += currentlyRotating * RotationRate * Time.deltaTime;
        newEulers.x = Mathf.Clamp(newEulers.x, 20, 70);
        transform.rotation = Quaternion.Euler(newEulers);
        

        if (newEulers.x == 20)
            currentlyRotating = 1;
        if (newEulers.x == 70)
            currentlyRotating = -1;
    }

    private void HandlePlayerInput()
    {
        rotationChange = 0;
        if (Input.GetKey(KeyCode.Q))
        {
            rotationChange = 1;
        }
        if (Input.GetKey(KeyCode.E))
        {
            rotationChange = -1;
        }

        if (rotationChange != 0)
        {
            Vector3 newEulers = transform.rotation.eulerAngles;
            newEulers.x += rotationChange * RotationRate * Time.deltaTime;
            newEulers.x = Mathf.Clamp(newEulers.x, 20, 70);
            transform.rotation = Quaternion.Euler(newEulers);
        }
    }



    public void RigidbodyInsertedIntoCannon(Rigidbody rbody)
    {
        if (RigidbodiesInCannon.Contains(rbody))
            return;
        RigidbodiesInCannon.Add(rbody);
    }

    public void RigidbodyLeftCannon(Rigidbody rbody)
    {
        if (RigidbodiesInCannon.Contains(rbody))
        {
            RigidbodiesInCannon.Remove(rbody);
        }
    }

    public void Fire()
    {
        foreach (Rigidbody rBody in RigidbodiesInCannon)
        {
            rBody.AddForce(ShotOrigin.forward * FirePower, ForceMode.Impulse);
            FlyingBodyController flyingBody = rBody.GetComponent<FlyingBodyController>();
            if (flyingBody != null)
            {
                MainController.Get.SetFollowingRagdollPhase(flyingBody);
                flyingBody.StartedFlying();
            }
        }
    }
}
