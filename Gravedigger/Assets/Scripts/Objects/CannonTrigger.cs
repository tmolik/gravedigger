﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonTrigger : MonoBehaviour
{
    public CannonController Cannon;

    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody != null)
        {
            Cannon.RigidbodyInsertedIntoCannon(other.attachedRigidbody);
        }       
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.attachedRigidbody != null)
        {
            Cannon.RigidbodyLeftCannon(other.attachedRigidbody);
        }
    }
}
