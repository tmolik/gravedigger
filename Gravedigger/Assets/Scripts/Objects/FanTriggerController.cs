﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanTriggerController : MonoBehaviour
{
    public FanController Fan;

    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody != null)
        {
            Fan.RigidbodyInRange(other.attachedRigidbody);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.attachedRigidbody != null)
        {
            Fan.RigidbodyLeftRange(other.attachedRigidbody);
        }
    }
}
