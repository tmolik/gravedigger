﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanController : MonoBehaviour
{
    public Transform RotatingPartsTransform;

    public float RotationRate = 100;

    public float AddedForce = 50;

    private List<Rigidbody> RigidbodiesInRange = new List<Rigidbody>();


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        RotatingPartsTransform.Rotate(new Vector3(0, 0, RotationRate * Time.deltaTime));
    }

    public void FixedUpdate()
    {
        foreach (Rigidbody rBody in RigidbodiesInRange)
        {
            rBody.AddForce(transform.forward * AddedForce);
        }
    }

    public void RigidbodyInRange(Rigidbody rbody)
    {
        if (RigidbodiesInRange.Contains(rbody))
            return;
        RigidbodiesInRange.Add(rbody);
    }

    public void RigidbodyLeftRange(Rigidbody rbody)
    {
        if (RigidbodiesInRange.Contains(rbody))
        {
            RigidbodiesInRange.Remove(rbody);
        }
    }

}
