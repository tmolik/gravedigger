﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampolineController : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        FlyingBodyController flyingBody = other.GetComponent<FlyingBodyController>();

        if (flyingBody != null)
        {
            flyingBody.AddTrampolinePower();
        }
    }
}
