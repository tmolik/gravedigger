﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaloonController : MonoBehaviour
{
    public float RisePower = 5;

    public Transform BaloonLineStart;

    private LineRenderer baloonLineRenderer;
    private Rigidbody baloonRigidbody;
    private ConfigurableJoint baloonJoint;


    // Start is called before the first frame update
    void Start()
    {
        baloonRigidbody = GetComponent<Rigidbody>();
        baloonJoint = GetComponent<ConfigurableJoint>();

        baloonLineRenderer = GetComponentInChildren<LineRenderer>();
        baloonLineRenderer.positionCount = 2;
    }

    // Update is called once per frame
    void Update()
    {

        // baloonRigidbody.AddForce(Vector3.up * RisePower);
        UpdateLineRenderer();
    }

    public void FixedUpdate()
    {
        
        if (baloonRigidbody.velocity.y < 10)
            baloonRigidbody.AddForce(Vector3.up * RisePower);

    }

    private void UpdateLineRenderer()
    {
        baloonLineRenderer.SetPosition(0, BaloonLineStart.position);
        baloonLineRenderer.SetPosition(1, baloonJoint.connectedBody.transform.position);
    }
}
