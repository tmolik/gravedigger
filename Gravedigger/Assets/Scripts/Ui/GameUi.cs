﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUi : MonoBehaviour
{
    public static GameUi Get { get; private set; }

    public GameUi()
    {
        Get = this;
    }

    public Button CannonButton;
    public Button ResetButton;

    public void Start()
    {
        CannonButton.gameObject.SetActive(true);
        ResetButton.gameObject.SetActive(false);
    }


    public void ClickedCannonButton()
    {
        if (!MainController.Get.Cannon.IsRotatingByItself)
        {
            MainController.Get.Cannon.IsRotatingByItself = true;
        }
        else
        {
            MainController.Get.Cannon.Fire();
            MainController.Get.Cannon.IsRotatingByItself = false;
            CannonButton.gameObject.SetActive(false);
        }
    }

    public void ClickedResetButton()
    {
        ResetButton.gameObject.SetActive(false);
        MainController.Get.SetLookingAtCannonCamera();
        MainController.Get.SpawnNewFlyingBody();
        CannonButton.gameObject.SetActive(true);
    }

    public void EnableResetButton()
    {
        ResetButton.gameObject.SetActive(true);
    }

}
